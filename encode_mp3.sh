#!/bin/sh

for f in $*
do
	ffmpeg -i "$f" -c:a libmp3lame -ac 2 -q:a 2 "$f".mp3
done
