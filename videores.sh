#!/usr/local/bin/ksh

DEFAULT_RES='480'
USAGE="Uso: $0 [-af] [-pXXX] archivo(s)\n\t-a para animación.\n\t-f forzar independientemente de las dimensiones.\n\t-rXXXp indica la resolución (480 por defecto). Ej.: -r720p."

if [ $# -eq 0 ]; then
	printf "$USAGE"
	exit 1
fi

counter=0
for param in "$@"; do
	if [[ ${param:0:1} == '-' ]]; then
		counter=$((counter+1))
		case ${param:1:1} in
			f)
				echo "f"
				;;
			a)
				echo "a"
				;;
			r)
				echo "r"
				;;
			*)
				printf "%s: Parámetro desconocido.\n" $param
				exit 2
				;;
		esac
	else
		break
	fi
done
shift $counter

exit 0

for f in "$@"; do
	if [ -e "$f" ]; then
		target="$f".480p.mkv
		case "$(file -b --mime-type "$f")" in
			video/*)
				width=`ffprobe -v error -select_streams v:0 -show_entries stream=width -of csv=s=x:p=0 "$f"`
				height=`ffprobe -v error -select_streams v:0 -show_entries stream=height -of csv=s=x:p=0 "$f"`
				printf "%s.\nLas dimensiones del video son %dx%d.\n" "$f" $width $height
				if [ -z $force ]; then
					if [ $width -gt $height -a $width -gt 854 ]; then
						printf "Procesando...\n"
						ffmpeg -hide_banner -loglevel error -i "$f" -c:s copy -vf "scale=854:-2" -max_muxing_queue_size 2048 -tune animation -map 0 "$target"
						if [ $? -eq 0 ]; then
							rm "$f"
						fi
					elif [ $height -gt $width -a $height -gt 854 ]; then
						printf "Procesando...\n"
						ffmpeg -hide_banner -loglevel error -i "$f" -c:s copy -vf "scale=854:-2" -max_muxing_queue_size 2048 -tune animation -map 0 "$target"
						if [ $? -eq 0 ]; then
							rm "$f"
						fi
					else
						printf "Las dimensiones del archivo ya son iguales o inferiores a 480p.\n"
					fi
				else
					ffmpeg -hide_banner -loglevel error -i "$f" -c:s copy -vf "scale=854:-2" -max_muxing_queue_size 2048 -tune animation -map 0 "$target"
				fi
			;;
			*)
				printf "No es un archivo de video: %s\n" "$f"
			;;
		esac
	fi
done			
