#!/bin/sh

if [ "$1" == "-e" ]; then
	tempfile="$TMP/dialog.tmp"

	dialog --clear --title "IDE" \
		--menu "Seleccione proyecto:" 10 60 300 \
		1 "General" \
		2 "~/bin" \
		3 "SWD" 2> $tempfile

	selection=`cat $tempfile`
	rm $tempfile
	case $selection in
		1)
			cd $HOME/proyectos/src
			;;
		2)
			cd $HOME/bin
			;;
		3)
			cd $HOME/proyectos/src/www-dev/swd-dev
			gruntless=1
			;;
	esac

	#urxvtcd -cd ${HOME}/proyectos/www-dev/test/ -e vim test.php
	tmux has-session -t ide 2>/dev/null
	if [ "$?" -eq 1 ] ; then
		tmux new -s ide -d
		tmux new-window -t ide:1 -n "Terminal"
		tmux kill-window -t ide:0
		if [ "$gruntless" -eq 1 ]; then
			tmux new-window -t ide:2 -n "Gruntless"
			case $selection in
				3)
					tmux send-keys -t ide:2 "grunt watch" C-m
					;;
			esac
		fi
		tmux new-window -t ide:5 -n "IDE"
		tmux send-keys -t ide:5 "vim --servername ide" C-m
	fi


	tmux select-window -t ide:2
	tmux -2 attach-session -t ide
elif [ "$1" == "-d" ]; then
	vim --servername ide --remote-send $'\e:wqa\n'
	tmux kill-session -t ide
else
	echo "Llamar con -e para activar o -d cerrar sesión."
fi
