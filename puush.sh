#!/bin/sh

FILE="/tmp/puush$$.jpg"  #Temporal file for the image

if [ -z "$PUUSH_API_KEY" ]; then
  echo "Set the variable PUUSH_API_KEY"
  exit 1
fi

if [ -n "$1" ]; then
	case "$1" in
		-s)
			scrot -q 100 -s $FILE
			;;
		-a)
			scrot -q 100 $FILE
			;;
		*)
			FILE=$1
			NO_TMP_FILE=1
			;;
	esac
else
	scrot -q 100 $FILE
fi

[ ! -f $FILE ] && echo File $FILE is not valid. && exit 2

#Upload to puush and store returned url or error in url variable
url=$(curl --http1.1 "https://puush.me/api/up" -# -F "k=$PUUSH_API_KEY" -F "z=poop" -F "f=@$FILE" | sed -E 's/^.+,(.+),.+,.+$/\1/' | tr -d \\n )
[ -z $NO_TMP_FILE ] && rm $FILE 

#If url variable is not empty and not an error, play success sound and echo url
if [ $url ]; then
	if [ $url != "-1," ]; then
		echo ${url:+`echo $url>$HOME'/.puush.tmp'`} $url
		cat $HOME'/.puush.tmp' | tr -d \\n | xclip
		rm $HOME'/.puush.tmp'
		mpg123 $HOME/multimedia/sonidos/robot_servo_arm.mp3 > /dev/null 2>&1
		exit 0
	#Warn in case of error and exit
	else
		echo "Puush failed to upload."
		mpg123 $HOME/multimedia/sonidos/wookie_noise.mp3 > /dev/null 2>&1
		exit 3
	fi
fi
mpg123 $HOME/multimedia/sonidos/pikachu-thunderbolt.mp3 > /dev/null 2>&1
