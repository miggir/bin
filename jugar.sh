#!/bin/sh

#PATHTOHERE="$HOME/bin/jugar.sh"

if [ -z "$WINE_GAMES_DIR" ]; then
	printf "Debes definir el directorio donde se encuentran los juegos de windows con \$WINE_GAMES_DIR.\n"
	exit 1
fi

if [ -z "$DOS_GAMES_DIR" ]; then
	printf "Debes definir el directorio donde se encuentran los juegos de ms-dos con \$DOS_GAMES_DIR.\n"
	exit 1
fi


#if [ $0 != "$PATHTOHERE" ]; then
#	# Script is being sourced, hence game must be windows and $GAMEEXEC set.
#	executable=`basename "$WINE_GAMES_DIR"/"$GAMEEXEC"`
#	install_dir=`dirname "$WINE_GAMES_DIR"/"$GAMEEXEC"`
#
#	cd $install_dir
#	printf "WINEPREFIX is %s\n." $WINEPREFIX
#	if [ "$WINEPREFIX" == "$HOME/.wine64" ]; then
#		# Execute special commands for 64bits wine
#		PATH=/opt/wine-5.0/usr/local/bin:$PATH
#		WINEPREFIX="$WINEPREFIX" WINEARCH="$WINEARCH" wine64 "$executable"
#		exit 0
#	else
#		# Assume it's a regular 32bits application.
#		WINEPREFIX="$WINEPREFIX" WINEARCH="$WINEARCH" wine "$executable"
#		exit 0
#	fi
#else
	# Direct call, hence require a game name if not set in $1. Display menu list?
	clear
	printf "Elige un juego:\n\n"
	cd "$DOS_GAMES_DIR"
	game_number=0
	for game_name in *; do
		game_number=$(($game_number+1))
		printf "\t%d) %s\n" $game_number "$game_name"
	done
	cd "$WINE_GAMES_DIR"
	for game_name in *; do
		game_number=$(($game_number+1))
		printf "\t%d) %s\n" $game_number "$game_name"
	done
	cd "$DOS_GAMES_DIR"
	game_number=0
	printf "\n Número: "
	read selected_game
	for game_dir in *; do
		# Dosbox games
		game_number=$(($game_number+1))
		if [ $game_number -eq $selected_game ]; then
			# Start selected game
			printf " Iniciando juego MS-DOS %s ...\n" "$game_dir"
			if [ -e $HOME/.dosbox/"$game_dir"-dosbox.conf ]; then
				dosbox -conf $HOME/.dosbox/"$game_dir"-dosbox.conf
				exit 0
			else
				printf "Archivo de configuración %s no existe.\n" $HOME/.dosbox/"$game_dir"-dosbox.conf
				exit 2
			fi
		fi
	done
	cd "$WINE_GAMES_DIR"
	for game_dir in *; do
		# Wine games
		game_number=$(($game_number+1))
		if [ $game_number -eq $selected_game ]; then
			# Start selected game
			absgamedir="$WINE_GAMES_DIR"/"$game_dir"/
			cd "$absgamedir"
			if [ -e "$absgamedir"winecfg.sh ]; then
				. "$absgamedir"winecfg.sh
				printf " Iniciando juego de Windows %s con ejecutable %s...\n" "$game_dir" "$GAMEEXE"
				if [ "$WINEARCH" == "win64" ]; then
					# Execute special commands for 64bits wine
					PATH=/opt/wine-5.0/usr/local/bin:$PATH
					WINEPREFIX="$WINEPREFIX" WINEARCH="$WINEARCH" wine64 "$GAMEEXE"
					exit 0
				else
					# Assume it's a regular 32bits application.
					WINEPREFIX="$WINEPREFIX" WINEARCH="$WINEARCH" wine "$GAMEEXE"
					exit 0
				fi
				exit 0
			else
				printf "Configuration file %s doesn't exit.\n" "$absgamedir"winecfg.sh
				printf "Creating a skeleton one if possible. You will have to edit it.\n"
				sleep 5
				echo "# Nombre del ejecutable relativo al directorio de instalación del juego." >> "$absgamedir"winecfg.sh
				echo "# Cambiar el prefijo a \$HOME/.wine64 y la arquitectura a win64 si el" >> "$absgamedir"winecfg.sh
				echo "# ejecutable es de 64-bit." >> "$absgamedir"winecfg.sh
				echo "GAMEEXE=\"nombre del ejecutable aqui\"" > "$absgamedir"winecfg.sh
				echo "WINEPREFIX=\"\$HOME/.wine32\"" >>  "$absgamedir"winecfg.sh
				echo "WINEARCH=\"win32\"" >>  "$absgamedir"winecfg.sh
				vi "$absgamedir"winecfg.sh
				exit 2
			fi
		fi
	done

#fi

# Set screen config to default in case the game doesn't restore it.
sh ~/bin/reset_resolution.sh




