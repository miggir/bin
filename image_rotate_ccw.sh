#!/bin/sh

if [ $# -eq 0 ]; then
    echo "No arguments provided"
    exit 1
fi

for f in ''$*''
do
	case "$(file -b --mime-type "$f")" in
		image/*)
			if ! command -v mogrify >/dev/null 2>&1; then
				if ! command -v convert >/dev/null 2>&1; then
					echo "No se encuentran ni mogrify ni convert"
					exit 2
				else
					echo "Converting $f 90º clockwise."
					convert -rotate "-90" "$f" "$f".out
					mv "$f".out "$f"
				fi
			else
				echo "Mogrifying $f 90º clockwise."
				mogrify -rotate "-90" "$f"
			fi
			;;
		*)
			echo "Not an image file: $f"
			exit 3
			;;
		esac
done
