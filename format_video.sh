#!/bin/sh

check_bframes_ok()
{
	result=`ffmpeg -y -hide_banner -v info -i "$1" -ss 0 -t 1 -an -c:v mpeg4 -f null /dev/null 2>&1 | grep mpeg4_unpack_bframes`
	if [ -z "$result" ]; then
		return 0 # B-frames are okay.
	else
		return 1 # B-frames need fixing.
	fi
}

USAGE="Uso: $0 [-f] archivos...
	-f para forzar."

if [ "$1" == "-f" ]; then
	force=1
	shift
fi

if [ $# -eq 0 ]; then
	echo "$USAGE"
	exit 1
fi

## MENU OPCIONES
#
tempfile="$TMP/dialog.tmp"
dialog --clear \
	--menu "Resolución:" 10 60 300 \
	1 "~480p 16:9" \
	2 "720p (HD)" \
	3 "1080p (Full HD)" 2> $tempfile

resolucion=`cat $tempfile`
rm $tempfile
dialog --clear --default-button no \
	--yesno "Dibujos animados?" 10 60
animation=0
if [ $? -eq 0 ]; then
	animation=1
fi

## CONFIGURACION PARAMETROS segun opciones elegidas
#
case $resolucion in
	1)	# 854x480
		vidmode="480p"
		scale_str="scale=854:-2"
		max_width=854
		;;
	2)	# 1280x720
		vidmode="720p"
		scale_str="scale=1280:-2"
		max_width=1280
		;;
	3)	# 1920x1080
		vidmode="1080p"
		scale_str="scale=1920:-2"
		max_width=1920
		;;
esac

if [ $animation -eq 1 ]; then
	tune_str="-tune animation"
fi

## PROCESADO DE ARCHIVOS
#
for f in "$@"
do
	if [ -e "$f" ]; then
		target="$f"."$vidmode"-HEVC.mkv
		case "$(file -b --mime-type "$f")" in
			video/*)
				check_bframes_ok "$f"
				check_bframes_return=$?
				if [ $check_bframes_return -eq 1 ]; then
					printf "El video usa packed B-frames. Arreglando...\n"
					ffmpeg -hide_banner -loglevel error -i "$f" -bsf:v mpeg4_unpack_bframes -vcodec copy -map 0 "temp-$f"
					mv "temp-$f" "$f"
					printf "Continuando...\n"
				fi
				width=`ffprobe -v error -select_streams v:0 -show_entries stream=width -of csv=s=x:p=0 "$f"`
				height=`ffprobe -v error -select_streams v:0 -show_entries stream=height -of csv=s=x:p=0 "$f"`
				printf "%s.\nLas dimensiones del video son %dx%d.\n" "$f" $width $height
				if [ -z $force ]; then
					if [ $width -gt $height -a $width -gt $max_width ]; then
						printf "Procesando...\n"
						ffmpeg -hide_banner -i "$f" -c:s copy -c:v libx265 -vtag hvc1 -vf "$scale_str" -max_muxing_queue_size 2048 $tune_str -map 0 "$target"
						if [ $? -eq 0 ]; then
							rm "$f"
						fi
					elif [ $height -gt $width -a $height -gt $max_width ]; then
						printf "Procesando...\n"
						ffmpeg -hide_banner -i "$f" -c:s copy -c:v libx265 -vtag hcv1 -vf "$scale_str" -max_muxing_queue_size 2048 $tune_str -map 0 "$target"
						if [ $? -eq 0 ]; then
							rm "$f"
						fi
					else
						printf "Las dimensiones del archivo ya son iguales o inferiores a $vidmode.\n"
					fi
				else
					printf "Procesando...\n"
					ffmpeg -hide_banner -i "$f" -c:s copy -c:v libx265 -vtag hvc1 -vf "$scale_str" -max_muxing_queue_size 2048 $tune_str -map 0 "$target"
					if [ $? -eq 0 ]; then
						rm "$f"
					fi
				fi
			;;
			*)
				printf "No es un archivo de video: %s\n" "$f"
			;;
		esac
	fi
done			
