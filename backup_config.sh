#! /bin/sh

# User configuration backup
FILENAME=`hostname`_`whoami`.backup_userconfig-`date +%Y%m%d`
TMP=$HOME'/tmp/'
LIST_OF_FILES='
	.fehbg
	.login
	.login_conf
	.mail_aliases
	.mailrc
	.profile
	.pypanelrc
	.shrc
	.Xdefaults
	.xsession
	.config/autostart
	.config/bin
	.config/background.jpg
	.config/conky/conky.conf
	.config/i3/config
	.config/fontconfig
	.config/mc/ini
	.config/mc/mc.ext
	.config/openbox/menu.xml
	.config/openbox/rc.xml
	.config/openbox/autostart
	.config/openbox/environment
	.config/user-dirs.dirs
	.config/mimeapps.list
	.local/share/applications
	.9menu
	.dosbox/*.conf
	.irssi/config
	.irssi/irssi.quit
	.irssi/*.theme
	.irssi/scripts
	.mplayer/config
	.mplayer/input.conf
	.ncmpcpp/config
	.ncmpcpp/bindings
	.newsboat/urls
	.newsboat/config
	.tmux.conf
	.vimrc
	.vim/bundle/scripts_list.txt
	.worker/config
	.worker/bookmarks
'

cd $HOME
ls .vim/bundle > .vim/bundle/scripts_list.txt
for file in $LIST_OF_FILES; do
	tar -rvpf "$TMP""$FILENAME".tar "$file"
done

echo "Comprimiendo..."

if [ `command -v bzip2` ]; then
	bzip2 "$TMP""$FILENAME".tar
	COMPRESS_EXT=.bz2
elif [ `command -v gzip` ]; then
	gzip "$TMP""$FILENAME".tar
	COMPRESS_EXT=.gz
fi

echo Backed up to "$TMP""$FILENAME""$COMPRESS_EXT"
