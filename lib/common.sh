#!/bin/sh

getChar () {
    local oldstty
    oldstty=$(stty -g)
    stty -icanon -echo min 1 time 0
    dd bs=1 count=1 2>/dev/null
    stty "$oldstty"
}

checkMount () {
	if [ -z "$1" ]; then
		printf "No mount point provided for checking in checkMount()\n"
		return 2
	fi

	printf "Testing if %s is mounted: " $1
	if [ `/sbin/mount | awk '{print $3}' | grep "$1" | wc -c` -ne 0 ]; then
		# Match found in at least one line.
		if [ "`/sbin/mount | awk '{print $3}' | grep "$1"`" = $1 ]; then
			# Exact match
			printf "Mounted.\n"
			return 0
		fi
		printf "Not mounted.\n"
		return 1
	else
		printf "Not mounted.\n"
		return 1
	fi
}
