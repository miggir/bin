#! /bin/sh

# Remove broken symlinks from the pkg backup of libs.
cd /usr/local/lib/compat/pkg
find . -type l -exec test ! -e {} \; -delete
