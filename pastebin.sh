#!/bin/sh

API_DEV_KEY="7bdafa468c9d0ce1366704007b691b13"
API_EXPIRE_DEFAULT="1D" #N=Never 10M=10Minutes 1H=1Hour 1D=1Day 1W=1Week 2W=2Weeks 1M=1Month 
API_PRIVATE="1" #0=public 1=unlisted 2=private
API_PASTE_NAME_DEFAULT=
API_FORMAT_DEFAULT=raw #raw directly links to the raw text. For accepted highlighting formats: http://pastebin.com/api#5

# -x expire time
# -t title
# -f format
# -c use clipboard

if [ -z "$API_DEV_KEY" ]; then
  echo "Set the variable API_DEV_KEY"
  exit 1
fi

while $@

[ ! -f $FILE ] && echo File $FILE is not valid. && exit 2

#Upload to pastebin and store returned url or error in url variable
url=$(curl -d api_option=paste&api_dev_key=${API_DEV_KEY}&api_paste_code=${PASTE}&api_paste_private=${API_PRIVATE}&api_paste_name=${API_PASTE_NAME_DEFAULT}&api_paste_expire_date=${API_EXPIRE_DEFAULT}&api_paste_format=${API_FORMAT_DEFAULT} "http://pastebin.com/api/api_post.php" )

#If url variable is not empty and not an error, play success sound and echo url
if [ $url ]; then
	if [ $url != "-1," ]; then
		echo ${url:+`mpg123 $HOME/media/sonidos/robot_servo_arm.mp3 > /dev/null 2>&1 && echo $url>$HOME'/.puush.tmp'`} $url
		cat $HOME'/.puush.tmp' | tr -d \\n | xclip
		rm $HOME'/.puush.tmp'
		exit 0
	#Warn in case of error and exit
	else
		mpg123 $HOME/media/sonidos/sw.r2d2error.mp3 > /dev/null 2>&1
		echo "Puush failed to upload."
		exit 3
	fi
fi
