#!/bin/sh

. ~/bin/lib/common.sh

if [ -z "$CDROM_MOUNT" ]; then
	printf "Debes definir el punto de montaje para el CD con \$CDROM_MOUNT\n"
	exit 1
fi

# Comprobar si hay algo montado en CDROM_MOUNT
checkMount "$CDROM_MOUNT"
cdmount_status=$?

# Intentar desmontar si hay algo en CDROM_MOUNT.
if [ $cdmount_status -eq 0 ]; then
	umount "$CDROM_MOUNT"
	if [ $? -ne 0 ]; then
		printf "El punto de montado %n está ocupado.\n" "$CDROM_MOUNT"
		exit 2
	fi
fi

if [ -f "$1" ]; then
	# Si la unidad ya esta abierta y montada, desmontar.
	unit=`mdconfig -l -f "$1"`
	if [ -z $unit ]; then
		# Archivo no abierto aun. Abrir y montar.
		unit=$(mdconfig -a -t vnode "$1")
		mount -t cd9660 /dev/$unit "$CDROM_MOUNT"
		if [ $? -eq 0 ]; then
			printf "Archivo %s como %s montado con éxito en %s.\n" "$1" $unit "$CDROM_MOUNT"
			exit 0
		else
			printf "Archivo %s como %s no se pudo montar en %s.\n" "$1" $unit "$CDROM_MOUNT"
			mdconfig -d -u $unit
			exit 2
		fi
	else
		# Archivo ya abierto. Desmontar y cerrar.
		# Aqui se podria comprobar que no este en uso montado en otro sitio y
		# demas. Pero parece necesario para el uso de este script de momento.
		umount /dev/$unit
		mdconfig -d -u $unit
		printf "Archivo %s como %s desmontado con éxito de %s y cerrado.\n" "$1" $unit "$CDROM_MOUNT"
		exit 0
	fi
else
	printf "No se ha expecificado un archivo o éste no existe.\n"
	exit 1
fi
