#!/bin/sh

if IP=$(curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//' | tr -d \\n); then
	echo $IP
	exit 0
else
	exit 1
fi
