#!/bin/sh


MUA=/usr/local/bin/claws-mail
CONFIG_DIR=config/claws-mail

if [ -z $CRYPTOHOME ]; then
	printf "\$CRYPTOHOME no está definido. No sé dónde encontrar la configuración del MUA.\n"
	exit 10
fi

if [ -d $CRYPTOHOME/$CONFIG_DIR ]; then
	$MUA &
	exit 0
else
	printf "Se necesita desencriptar la partición segura.\n"
	sh $HOME/.config/bin/vaulthandler.sh -e
	if [ $? -ne 0 ]; then
		printf "No se puede acceder al correo.\n"
		zenity --error --text="No se puede acceder al correo. Comprueba que la partición segura ha sido desencriptada y montada." --title="Aviso\!"
		exit $?
	else
		$MUA &
		exit 0
	fi
fi	
