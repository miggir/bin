#!/usr/local/bin/ksh93

######################### CONFIG ###############################################
NLINES=50 #Lines to include from the bottom of the log.
DISCORDMARK="Ð " #Symbol added before a nick to indicate that it's from discord. ~line 33.
TEMPFILE=$TMP/logcowsay

# Tags to enclose nicknames.
TAGOPEN="<"
TAGCLOSE=">"

# Cow assignation to nick(s)
typeset -A COWS
COWS=([@Ten]=elephant-in-snake.cow
	[@Twelve]=elephant-in-snake.cow
	[@Thirteen]=elephant-in-snake.cow
	[Ð Cherokee]=turkey.cow
	[Ð Mikel]=milk.cow
	[Ð LahasaFy]=stegosaurus.cow
	[Ð Taeno`dess`astra]=supermilker.cow
	[Ð Baugrems]=hellokitty.cow
	[.Loki]=skeleton.cow
	[Loki]=skeleton.cow
	[Ð Amirantha Dahrii]=supermilker.cow
	[Ð SpaceAnubis]=mutilated.cow
	[Ð LucioGhosty]=
	[Ð Kez]=three-eyes.cow
	[+blackhole]=moose.cow)
#################### END CONFIG ################################################

clear
if [ -z "$1" ]; then
	printf "You must specify an irc log file.\n"
	exit 1
elif [ ! -f "$1" ]; then
	printf "File %s doesn't exist.\n" "$1"
	exit 2
fi

# Only work on the number of lines as defined by NLINES.
tail -n $NLINES "$1" > "$TEMPFILE"

# Prepare the temp file for adecuate processing.
# Remove unwanted garbage lines. (BSD sed syntax.)
sed -i '' '/^---/d' "$TEMPFILE" #Removes system notifications from irssi logs.
sed -i '' '/UB3R-B0T/d' "$TEMPFILE" #Removes UB3R-B0T from irssi logs.
sed -i '' "s/$TAGOPEN@swc-discord$TAGCLOSE $TAGOPEN/$TAGOPEN$DISCORDMARK/g" "$TEMPFILE" #Replace discord bot's tag.
sed -i '' 's/\*/\\*/g' "$TEMPFILE" #Escape *.

# Group consecutive lines from the same nick into one line with linebreaks.
nickprev=""
rm "${TEMPFILE}"2.txt
while IFS="" read -r LINE
do
	nick=$(echo ${LINE} | cut -d"$TAGCLOSE" -f1 | cut -d"$TAGOPEN" -f2)
	text=$(echo ${LINE} | cut -d"$TAGCLOSE" -f2)
	if [ "$nick" != "$nickprev" ]; then
		printf "\n%c%s%c %s" $TAGOPEN "$nick" $TAGCLOSE "$text" >> "${TEMPFILE}"2.txt
	else
		printf "\\\n %s" "$text" >> "${TEMPFILE}"2.txt
	fi
	nickprev="$nick"
done < "$TEMPFILE"
sed -i '' '1d' "${TEMPFILE}"2.txt #remove first line because it's just a NL.

# Get nick and all the text, including NL until next line with a nick and cowify it
while IFS="" read -r LINE
do
	#Parse differently if it's a message or an action. TODO. NOT WORKING
	if test "${LINE#*$TAGCLOSE}" != "$LINE"
		then
			nick=$(echo ${LINE} | cut -d"$TAGCLOSE" -f1 | cut -d"$TAGOPEN" -f2)
			text=$(echo ${LINE} | cut -d"$TAGCLOSE" -f2)
		else
			clear
			exit 0
			nick=$(echo ${LINE} | cut -d" " -f4)
			text=$(echo ${LINE} | cut -d" " -f5)
	fi
	if [ "${COWS["$nick"]}" != "" ]; then
		echo $nick: $text | cowsay -f "${COWS[$nick]}"  | lolcat
	else
		echo $nick: $text | cowsay | lolcat
	fi
done < "${TEMPFILE}"2.txt
