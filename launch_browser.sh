#!/bin/sh

DEFAULT_BROWSER=firefox
CONFIG_BASE_DIR=config

FIREFOX_GUEST_PROFILE=guest
FIREFOX_SECURE_PROFILE=secure

if [ $# -eq 0 ] || ! [ -e /usr/local/bin/"$1" ]; then
	browser=$DEFAULT_BROWSER
else
	browser="$1"
fi

if [ -z $CRYPTOHOME ]; then
	printf "\$CRYPTOHOME no está definido. No sé dónde encontrar los perfiles del navegador.\n"
	exit 10
fi
config_dir="$CRYPTOHOME"/"$CONFIG_BASE_DIR"/"$browser"
printf "Usando $config_dir\n"


sh $HOME/.config/bin/vaulthandler.sh -c
cryptostatus=$?

case "$browser" in
	"firefox")
		if [ $cryptostatus -eq 0 ]; then
			/usr/local/bin/firefox -P $FIREFOX_SECURE_PROFILE
		else
			/usr/local/bin/firefox -P $FIREFOX_GUEST_PROFILE
		fi
		;;
	"qutebrowser")
		/usr/local/bin/"$browser"
		;;
	*)
		echo "Navegador no aceptado."
		;;
esac

