#!/bin/sh

host=`echo "$1" | cut -d '?' -f 1`
queryurl=`echo "$1" | cut -d '?' -f 2`

youtube-dl --extract-audio --audio-format mp3 http://www.youtube.com?$queryurl
