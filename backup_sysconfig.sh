#! /bin/sh

# User configuration backup
FILENAME=`hostname`_`whoami`.backup_sysconfig-`date +%Y%m%d`
TMP=$HOME/
LIST_OF_FILES='
	etc/make.conf
	etc/csh.cshrc
	etc/fstab
	etc/sysctl.conf
	etc/rc.conf
	etc/periodic.conf
	etc/pf.conf
	etc/pf-www.conf
	etc/pf.banned
	etc/pf-block.conf
	etc/hosts.allow
	etc/login.conf
	etc/hosts
	etc/group
	etc/passwd
	etc/ssh/sshd_config
	etc/devfs.conf
	etc/devfs.rules
	boot/loader.conf
	usr/local/etc/hiawatha/hiawatha.conf
	usr/local/etc/hiawatha/index.xslt
	usr/local/etc/php.ini
	usr/local/etc/php-fpm.conf
	usr/local/etc/xinetd.conf
	usr/local/etc/musicpd.conf
	usr/local/etc/mysql/my.cnf
	usr/local/etc/sshguard.conf
'

cd /
for file in $LIST_OF_FILES; do
	tar -rvpf "$TMP""$FILENAME".tar "$file"
done


if [ `command -v bzip2` ]; then
	bzip2 "$TMP""$FILENAME".tar
	COMPRESS_EXT=.bz2
elif [ `command -v gzip` ]; then
	gzip "$TMP""$FILENAME".tar
	COMPRESS_EXT=.gz
fi

echo Backed up to "$TMP""$FILENAME""$COMPRESS_EXT"
