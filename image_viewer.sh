#!/bin/sh

DIR=`pwd`
cd /

if [ "$1" == "-c" ]; then
	feh --theme galeriasvg --start-at "$2" "$DIR"
else
	feh --theme galeria --start-at "$1" "$DIR"
fi
