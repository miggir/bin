#!/bin/sh

DESTDIR=$HOME'/tmp/cdrip'
FORMAT='o'

printf "Output format? (w)av, (o)gg or (m)p3. Default is ogg."
stty cbreak
#stty raw
FORMAT=`dd if=/dev/tty bs=1 count=1 2>/dev/null`
stty -cbreak
#stty -raw
printf "\nWhere to? (Default is %s)" "$DESTDIR"
read NEWDESTDIR

if [ ! -z "$NEWDESTDIR" ]; then
	DESTDIR=$NEWDESTDIR
	printf "New destination directory set to %s.\n" "$DESTDIR"
	if [ ! -d "$DESTDIR" ]; then
		mkdir $DESTDIR
		if [ $? -eq 0 ]; then
			printf "Directory created.\n"
		else
		    echo "Unable to create directory.\n"
		    exit 1
		fi
	fi
fi

cd $DESTDIR
cdparanoia -B -d /dev/cd0
for i in `ls *.wav`; do
	if [ "$FORMAT" = "m" ]; then
		printf "Encoding to mp3.\n"
		lame -m s "$i" -o "$(echo $i | awk '{print substr($0,6,2)}' -).mp3"
		rm -f "$i"
	elif [ "$FORMAT" = "o" ]; then
		printf "Encoding to ogg.\n"
		oggenc "$i"
		rm -f "$i"
	fi
done
exit 0
