#!/bin/sh

# Get maximun resolution.
#x=`xrandr | grep maximum | cut -d, -f3 | cut -d' ' -f3`
#y=`xrandr | grep maximum | cut -d, -f3 | cut -d' ' -f5`

maxres=`/usr/local/bin/xrandr | grep + | tail -1 | cut -d' ' -f4`
monitor=`/usr/local/bin/xrandr --listactivemonitors | grep 0 | rev | cut -d' ' -f1 | rev`

xrandr --output "$monitor" --mode "$maxres"

sh ~/.config/bin/brightmed.sh
