#!/bin/sh

# Config
maxsize=2000 # Process images with any side larger than this.
targetsize=1600 # Resize to this.

if [ -d "$1" ]; then
  dir="$1"
else
  dir=`pwd`
fi

#find . -name "*.jpg" -print0|xargs -I{} -0 mogrify -format jpg +distort srt "%[fx:(w>$maxsize || h>$maxsize)?0.5:1] 0" {}
#exit 0

for f in "$dir/"*
do
	if [ -f "$f" ]; then
		echo Checking file "$f"...
		width=`identify -format "%w" "$f"`
		height=`identify -format "%h" "$f"`
		if [ $width -gt $maxsize ] || [ $height -gt $maxsize ]; then
			echo Processing file.
			mogrify -resize "$maxsize>" "$f"
			#mogrify -format jpg +distort srt "%[fx:(w>$maxsize || h>$maxsize)?0.5:1] 0" "$f"
		fi
	fi
done
