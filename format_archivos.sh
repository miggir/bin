#!/bin/sh

# Substitution strings for renamex.
# First two are different... somehow.
LIST_OF_SUBSTITUTIONS='
	-s/_-_/./gi
	-s/_–_/./gi
	-s/.-././gi
	-s/._/./gi
	-s/__/_/gi
	-s/.././gi
	-s/-_/./gi
	-s/-././gi
	-s/_-/./gi
	-s/.-/./gi
	-s/_././gi
	-s/(/./gi
	-s/)/./gi
	-s/[/./gi
	-s/]/./gi
	-s/{/./gi
	-s/}/./gi
	-s/=//gi
'
# These filenames will get deleted.
LIST_OF_REMOVALS='
	thumbs.db
	desktop.ini
	folder.jpg
'
if [ -d "$1" ]; then
  DIR="$1"
else
  DIR=`pwd`
fi

if [ "$DIR" = "$HOME" ]; then
	echo "You should not do this in a home directory."
	exit 1;
fi

find "$DIR"/ -type d -exec chmod 755 {} \;
find "$DIR"/ -type f -exec chmod 644 {} \;

for f in $LIST_OF_REMOVALS; do
	find "$DIR" -name "$f" -delete -print
done

renamex -l -R "$DIR/"*
renamex -l -R -s/\ /_/gi "$DIR/"* #Spaces won't work as an element in the array.
for i in `seq 1 10`; do
	for s in $LIST_OF_SUBSTITUTIONS; do
		/usr/local/bin/renamex -A -R "$s" "$DIR/"*
	done
done
