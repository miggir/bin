#!/bin/sh

case "$1" in
	start)
		xdesktopwaves -quality 4 -storm 4 -rain 5 -watercolor black -skycolor purple -lightcolor red &
		mpg123 --loop -1 /home/miggir/multimedia/sonidos/lluvia.mp3 &
		;;
	stop)
		killall xdesktopwaves
		killall mpg123
		;;
	audio)
		killall xdesktopwaves
		killall mpg123
		mpg123 --loop -1 /home/miggir/multimedia/sonidos/lluvia.mp3 &
		;;
esac

