#!/bin/sh

SESSION=consola
tmux has-session -t $SESSION 2>/dev/null
if [ "$?" -eq 1 ] ; then
	tmux new -s $SESSION -d
	tmux selectp -t 0
	tmux splitw -v -p 60

	tmux selectp -t 1
        tmux send-keys -t $SESSION:1 "clear" C-m
        tmux send-keys -t $SESSION:1 "clear" C-m
	tmux splitw -h -p 50

	tmux selectp -t 0 
	tmux send-keys -t $SESSION:1 "clear" C-m
        tmux send-keys -t $SESSION:1 "tmux display-panes && clear" C-m
	tmux selectp -t 3
        tmux send-keys -t $SESSION:1 "clear && fortune|cowsay" C-m
fi
tmux -2 attach-session -t $SESSION
