#!/bin/sh

if [ "$1" == "-e" ]; then
	tmux has-session -t comms 2>/dev/null
	if [ "$?" -eq 1 ] ; then
		tmux new -s comms -d
	#	tmux rename-window -t comms:1 "Terminal"
		tmux new-window -t comms:5 -n "Correo"
		tmux new-window -t comms:2 -n "IRC"
		tmux new-window -t comms:3 -n "(logs)"
		tmux new-window -t comms:4 -n "RSS"

	#	tmux send-keys -t comms:2 "TERM=tmux-256color irssi" C-m
		tmux send-keys -t comms:2 "irssi" C-m
		tmux send-keys -t comms:3 "cd ~/personal/logs/irclogs/2021/SWCnet && clear" C-m
		tmux send-keys -t comms:4 "newsboat" C-m
	fi


	tmux select-window -t comms:2
	tmux -2 attach-session -t comms
elif [ "$1" == "-d" ]; then
	kill -s SIGHUP $(pgrep irssi)
	tmux kill-session -t comms
else
	echo "Llamar con -e para activar o -d cerrar sesión."
fi
